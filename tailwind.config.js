module.exports = {
  purge: ["./src/**/*.tsx"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        paper: "#f0f0f0",
      },
      gridTemplateColumns: {
        "input-group": "1fr 2fr",
        "icons-list": "minmax(4rem, auto) 1fr",
      },
      boxShadow: {
        input: "inset 3px 3px 6px #cccccc, inset -3px -3px 6px #ffffff",
        button: "5px 5px 10px #cccccc, -5px -5px 10px #ffffff",
      },
    },
  },
  variants: {
    extend: {
      textColor: ["active"],
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
