import { render } from "solid-js/web";

import "./index.css";
import App from "./App";

const mainElem = document.getElementById("rechaos");
if (!mainElem) {
  throw new Error("main element missing");
}

render(() => <App />, mainElem);
