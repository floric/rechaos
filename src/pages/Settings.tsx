import { Component } from "solid-js";
import { Dialog } from "../components/Dialog";

export const Settings: Component = () => (
  <Dialog route="settings" title="Settings" />
);
