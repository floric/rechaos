import { Component, For } from "solid-js";
import { Subtitle } from "../components/typography/Subtitle";
import { Dialog } from "../components/Dialog";

const attributions: Array<{ attribution: string; url: string }> = [
  {
    attribution: "Icons by CoreUI",
    url: "https://icons.coreui.io/",
  },
];

export const Attributions: Component = () => (
  <Dialog route="attributions" title="Attributions">
    <div>
      This App is based on a number of great libraries. I'd like to thank their
      creators for the awesome work!
    </div>
    <div class="mb-4" />
    <Subtitle>Libraries</Subtitle>
    <div class="grid grid-cols-icons-list gap-4">
      <For each={attributions}>
        {({ attribution, url }) => (
          <>
            <div class="justify-self-stretch">{attribution}</div>
            <div>
              <a class="font-light text-blue-600" href={url}>
                Website
              </a>
            </div>
          </>
        )}
      </For>
    </div>
  </Dialog>
);
