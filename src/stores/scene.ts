import { createStore } from "solid-js/store";
import { nanoid } from "nanoid";
import { events, EventType } from "../events";
import { createMemo } from "solid-js";

export enum ElementType {
  TEXT = "text",
  BOX = "box",
}

export type Vector2d = {
  x: number;
  y: number;
};

type Base = {
  id: string;
  title: string;
  shapeId: string;
  position: Vector2d;
};

const nodes = new Map<string, Node>();

export type TextNode = Base & {
  type: ElementType.TEXT;
  value: string;
  fontSize: number;
};

export type BoxNode = Base & {
  type: ElementType.BOX;
  width: number;
  height: number;
};

export type Node = BoxNode | TextNode;

export type Scene = {
  selection: string | null;
  hovered: string | null;
};

export const [scene, setScene] = createStore<Scene>({
  selection: null,
  hovered: null,
});

export const currentNode = createMemo<Node | null>(
  () => {
    if (!scene.selection) {
      return null;
    }
    return nodes.get(scene.selection) || null;
  },
  null,
  {
    equals: (a, b) => {
      if (a === null && b === null) {
        return true;
      } else if (a === null || b === null) {
        return false;
      }

      return a.id === b.id;
    },
  }
);

const createId = () => nanoid(8);

events.subscribe((ev) => {
  if (ev.type === EventType.NODE_SELECTED) {
    setScene(() => ({
      selection: ev.nodeId,
    }));
  } else if (ev.type === EventType.NODE_HOVERED) {
    setScene(() => ({
      hovered: ev.nodeId,
    }));
  } else if (ev.type === EventType.TEXT_EDITED) {
    createOrUpdateNode(ev.value);
  } else if (ev.type === EventType.BOX_EDITED) {
    createOrUpdateNode(ev.value);
  } else if (ev.type === EventType.NODE_EDITED) {
    createOrUpdateNode(ev.value);
  } else if (ev.type === EventType.ELEMENT_ADDED) {
    const id = createId();
    const shapeId = createId();
    if (ev.elementType === ElementType.TEXT) {
      const text: TextNode = {
        title: "Text",
        type: ElementType.TEXT,
        value: "Text",
        fontSize: 12,
        id,
        shapeId,
        position: ev.position,
      };
      createOrUpdateNode(text);
      events.next({ type: EventType.TEXT_CREATED, id, value: text });
    } else if (ev.elementType === ElementType.BOX) {
      const box: BoxNode = {
        title: "Box",
        type: ElementType.BOX,
        id,
        shapeId,
        position: ev.position,
        width: 100,
        height: 50,
      };
      createOrUpdateNode(box);
      events.next({ type: EventType.BOX_CREATED, id, value: box });
    }
  } else if (ev.type === EventType.NODE_DELETED) {
    deleteNode(ev.nodeId);
    if (scene.selection === ev.nodeId) {
      events.next({
        type: EventType.NODE_SELECTED,
        isExpanded: false,
        nodeId: null,
      });
    }
  }
});

const createOrUpdateNode = <T extends Node>(node: Partial<T>) => {
  if (!node.id) {
    return;
  }
  nodes.set(node.id, { ...(nodes.get(node.id) as T), ...node });
};

const deleteNode = (nodeId: string) =>
  setScene((val) => {
    const node = nodes.get(nodeId);
    if (!node) {
      return;
    }
    nodes.delete(node.id);
    const selection = val.selection !== node.id ? val.selection : null;
    return { ...val, selection, nodes };
  });
