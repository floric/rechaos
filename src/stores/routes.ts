import { createStore } from "solid-js/store";

export type DialogRoute = "settings" | "attributions" | null;

export const [routes, setRoutes] = createStore<{
  openDialog: DialogRoute;
}>({
  openDialog: null,
});

export const setDialog = (dialogRoute: DialogRoute): void =>
  setRoutes({ openDialog: dialogRoute });
