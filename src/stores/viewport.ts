import { createStore } from "solid-js/store";
import { debounceTime, filter, map } from "rxjs/operators";
import { merge } from "rxjs";

import { events, EventType, NodeSelectedEvent, UIEvent } from "../events";
import type { Vector2d } from "./scene";

export type Viewport = {
  padding: number;
  width: number;
  height: number;
  canvas: {
    zoom: number;
    center: Vector2d;
  };
  showZoom: boolean;
  isPropertiesOpen: boolean;
};

export const [viewport, setViewport] = createStore<Viewport>({
  padding: 10,
  width: 0,
  height: 0,
  isPropertiesOpen: false,
  canvas: {
    zoom: 1,
    center: { x: 0, y: 0 },
  },
  showZoom: false,
});

function isNodeSelectedEvent(ev: UIEvent): ev is NodeSelectedEvent {
  return ev.type === EventType.NODE_SELECTED;
}

// show properties when node gets selected
events.pipe(filter(isNodeSelectedEvent)).subscribe((ev) => {
  if (ev.nodeId !== null && ev.isExpanded) {
    openProperties();
  } else {
    closeProperties();
  }
});

// show zoom info during zoom until 2s after last zoom action
merge(
  events.pipe(
    filter((n) => n.type === EventType.ZOOM && !viewport.showZoom),
    map(() => true)
  ),
  events.pipe(
    filter((n) => n.type === EventType.ZOOM),
    debounceTime(1000),
    map(() => false)
  )
).subscribe((newValue) => {
  showShowZoom(newValue);
});

export const openProperties = (): void =>
  setViewport(() => ({ isPropertiesOpen: true }));

export const closeProperties = (): void =>
  setViewport(() => ({ isPropertiesOpen: false }));

export const setWidthAndHeight = (width: number, height: number): void =>
  setViewport(() => ({
    width,
    height,
  }));

export const setZoom = (zoom: number): void =>
  setViewport((val) => ({
    canvas: {
      ...val.canvas,
      zoom,
    },
  }));

export const setCenter = (center: Vector2d): void =>
  setViewport((val) => ({
    canvas: {
      ...val.canvas,
      center,
    },
  }));

export const showShowZoom = (newValue: boolean): void =>
  setViewport(() => ({
    showZoom: newValue,
  }));
