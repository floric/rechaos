import Konva from "konva";

const createCurve = (
  start: [number, number],
  middlePoints: Array<[number, number]>,
  end: [number, number]
): Konva.Group => {
  const distance = 50;

  const createBezierPoints = (
    current: [number, number],
    angle: number,
    type: "end" | "start"
  ) => {
    const factorX = Math.sin((angle / 360) * 2 * Math.PI);
    const factorY = Math.sin(((angle - 90) / 360) * 2 * Math.PI);
    return [
      ...(type === "start" ? current : []),
      ...[current[0] + factorX * distance, current[1] + factorY * distance],
      ...(type === "end" ? current : []),
    ];
  };

  const startPts = createBezierPoints(start, 125, "start");
  const endPts = createBezierPoints(end, 180, "end");

  const createMarker = (point: [number, number]): Array<number> => {
    const angle = 0;
    const factorX = Math.sin((angle / 360) * 2 * Math.PI);
    const factorY = Math.sin(((angle - 90) / 360) * 2 * Math.PI);
    return [
      ...[point[0] + factorX * distance, point[1] + factorY * distance],
      point[0],
      point[1],
      ...[point[0] - factorX * distance, point[1] - factorY * distance],
    ];
  };

  const bezierLine = new Konva.Line({
    bezier: true,
    points: [
      ...startPts,
      ...(middlePoints.map(createMarker).reduce((a, b) => [...a, ...b], []) ||
        []),
      ...endPts,
    ],
    strokeEnabled: true,
    stroke: "black",
  });
  const flatLine = new Konva.Line({
    points: [
      ...start,
      ...(middlePoints.reduce<Array<number>>((a, b) => [...a, ...b], []) || []),
      ...end,
    ],
    strokeEnabled: true,
    stroke: "#CCC",
  });

  const curve = new Konva.Group({ x: 0, y: 0 });
  curve.add(bezierLine);
  curve.add(flatLine);
  for (const [x, y] of middlePoints) {
    curve.add(
      new Konva.Circle({
        x,
        y,
        width: 2,
        height: 2,
        strokeEnabled: true,
        stroke: "orange",
      })
    );
  }
  curve.add(
    new Konva.Line({
      points: startPts,
      strokeEnabled: true,
      stroke: "red",
    })
  );
  curve.add(
    new Konva.Line({
      points: endPts,
      strokeEnabled: true,
      stroke: "blue",
    })
  );
  return curve;
};

export const curve = createCurve(
  [0, 0],
  [
    [50, 50],
    [200, 100],
    [150, 300],
  ],
  [100, 200]
);
