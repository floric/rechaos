import Konva from "konva";
import { events, EventType } from "../events";
import { viewport } from "../stores/viewport";
import type { Node } from "../stores/scene";
import { updateSelection } from "./selection";

export const updateNode = (
  group: Konva.Group,
  stage: Konva.Stage,
  elem: Partial<Node>
): void => {
  if (elem.position) {
    group.position({
      x: elem.position.x,
      y: elem.position.y,
    });
  }
  updateSelection(stage);
};

export const createDraggableGroup = (id: string): Konva.Group => {
  const group = new Konva.Group({
    id,
    draggable: true,
    dragBoundFunc: ({ x, y }) => ({
      x: Math.min(
        viewport.width - viewport.padding,
        Math.max(viewport.padding, x)
      ),
      y: Math.min(
        viewport.height - viewport.padding,
        Math.max(viewport.padding, y)
      ),
    }),
  });

  group
    .on("click", (ev) => {
      ev.cancelBubble = true;
      events.next({
        type: EventType.NODE_SELECTED,
        nodeId: group.id(),
        isExpanded: true,
      });
    })
    .on("mouseover", (ev: any) => {
      ev.cancelBubble = true;
      document.body.style.cursor = "pointer";
      events.next({ type: EventType.NODE_HOVERED, nodeId: group.id() });
    })
    .on("mouseout", (ev: any) => {
      ev.cancelBubble = true;
      document.body.style.cursor = "default";
      events.next({ type: EventType.NODE_HOVERED, nodeId: null });
    })
    .on("dragmove", (ev: any) => {
      ev.cancelBubble = true;
      document.body.style.cursor = "grabbing";
    })
    .on("dragend", () => {
      document.body.style.cursor = "default";

      events.next({
        type: EventType.NODE_EDITED,
        nodeId: group.id(),
        value: {
          id: group.id(),
          position: group.position(),
        },
      });
    });

  return group;
};
