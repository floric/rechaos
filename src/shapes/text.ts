import Konva from "konva";
import type { TextNode } from "../stores/scene";
import { createDraggableGroup, updateNode } from "./util";

export const createText = (
  id: string,
  shapeId: string
): { group: Konva.Group; text: Konva.Text } => {
  const group = createDraggableGroup(id);
  const text = new Konva.Text({
    id: shapeId,
    align: "center",
    verticalAlign: "middle",
  });
  group.add(text);

  return { group, text };
};

export const updateText = (
  group: Konva.Group,
  text: Konva.Text,
  stage: Konva.Stage,
  elem: TextNode
): void => {
  if (elem.value !== undefined) {
    text.text(elem.value);
  }
  if (elem.fontSize) {
    text.fontSize(elem.fontSize);
  }
  updateNode(group, stage, elem);
};
