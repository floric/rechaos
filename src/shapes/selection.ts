import Konva from "konva";
import { scene } from "../stores/scene";

const selectionOffset = 8;
const selectionColor = "#b380ff";
const hoverColor = "#FFF";

export const selectionId = "selection";
export const hoverId = "hovered";

type BorderType = "selection" | "hover";

export const createBorder = (
  elem: Konva.Group,
  type: BorderType
): Konva.Rect => {
  const { height, width, x, y } = elem.getClientRect({
    skipStroke: true,
    skipTransform: true,
  });

  return new Konva.Rect({
    id: type === "selection" ? selectionId : hoverId,
    x: x - selectionOffset,
    y: y - selectionOffset,
    width: width + selectionOffset * 2,
    height: height + selectionOffset * 2,
    stroke: type === "selection" ? selectionColor : hoverColor,
    cornerRadius: selectionOffset,
    strokeWidth: 2,
    strokeEnabled: true,
    dash: [7, 7],
    dashEnabled: true,
    listening: false,
  });
};

export const updateSelection = (stage: Konva.Stage): void => {
  if (!scene.selection) {
    clearSelection(stage, "selection");
    return;
  }

  if (scene.selection === scene.hovered) {
    clearSelection(stage, "hover");
  }
  clearSelection(stage, "selection");

  const selectedElem = stage.findOne<Konva.Group>(`#${scene.selection}`);
  const selectionBorder = createBorder(selectedElem, "selection");
  selectedElem.add(selectionBorder);
};

export const updateHover = (stage: Konva.Stage): void => {
  if (!scene.hovered) {
    clearSelection(stage, "hover");
    return;
  }

  clearSelection(stage, "hover");

  if (scene.selection === scene.hovered) {
    return;
  }

  const selectedElem = stage.findOne<Konva.Group>(`#${scene.hovered}`);
  const selectionBorder = createBorder(selectedElem, "hover");
  selectedElem.add(selectionBorder);
};

const clearSelection = (stage: Konva.Stage, type: BorderType) => {
  const oldSelection = stage.findOne(
    `#${type === "selection" ? selectionId : hoverId}`
  );
  oldSelection?.remove();
};
