import Konva from "konva";
import type { BoxNode } from "../stores/scene";
import { createDraggableGroup, updateNode } from "./util";

export const createBox = (
  id: string,
  shapeId: string
): { group: Konva.Group; box: Konva.Rect } => {
  const group = createDraggableGroup(id);
  const box = new Konva.Rect({
    id: shapeId,
    stroke: "black",
    strokeWidth: 2,
  });
  group.add(box);

  return { group, box };
};

export const updateBox = (
  group: Konva.Group,
  box: Konva.Rect,
  stage: Konva.Stage,
  elem: Partial<BoxNode>
): void => {
  if (elem.width) {
    box.width(elem.width || box.width());
  }
  if (elem.height) {
    box.height(elem.height || box.height());
  }
  updateNode(group, stage, elem);
};
