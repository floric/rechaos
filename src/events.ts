import { Subject } from "rxjs";
import type {
  Vector2d,
  TextNode,
  BoxNode,
  Node,
  ElementType,
} from "./stores/scene";

export enum EventType {
  NODE_SELECTED = "NodeSelected",
  NODE_HOVERED = "NodeHovered",
  TEXT_EDITED = "TextEdited",
  BOX_EDITED = "BoxEdited",
  NODE_EDITED = "NodeEdited",
  ELEMENT_ADDED = "ElementAdded",
  TEXT_CREATED = "TextCreated",
  BOX_CREATED = "BoxCreated",
  NODE_DELETED = "NodeDeleted",
  ZOOM = "Zoom",
}

export type NodeSelectedEvent = {
  type: EventType.NODE_SELECTED;
  nodeId: string | null;
  isExpanded: boolean;
};

export type NodeHoveredEvent = {
  type: EventType.NODE_HOVERED;
  nodeId: string | null;
};

export type ZoomedEvent = {
  type: EventType.ZOOM;
  zoom: number;
};

export type TextEditedEvent = {
  type: EventType.TEXT_EDITED;
  nodeId: string;
  value: TextNode;
};

export type BoxEditedEvent = {
  type: EventType.BOX_EDITED;
  nodeId: string;
  value: Partial<BoxNode>;
};

export type NodeEditedEvent = {
  type: EventType.NODE_EDITED;
  nodeId: string;
  value: Partial<Node>;
};

export type ElementAddedEvent = {
  type: EventType.ELEMENT_ADDED;
  elementType: ElementType;
  position: Vector2d;
};

export type TextCreatedEvent = {
  type: EventType.TEXT_CREATED;
  id: string;
  value: TextNode;
};

export type BoxCreatedEvent = {
  type: EventType.BOX_CREATED;
  id: string;
  value: BoxNode;
};

export type NodeDeletedEvent = {
  type: EventType.NODE_DELETED;
  nodeId: string;
};

export type UIEvent =
  | NodeSelectedEvent
  | NodeHoveredEvent
  | ZoomedEvent
  | TextEditedEvent
  | BoxEditedEvent
  | NodeEditedEvent
  | ElementAddedEvent
  | TextCreatedEvent
  | BoxCreatedEvent
  | NodeDeletedEvent;

export const events = new Subject<UIEvent>();

// Used only for debug logging
events.subscribe((val) => {
  const { type, ...remaining } = val;

  console.log(val.type, remaining);
});
