import { Component } from "solid-js";

export const TextInput: Component<{
  id: string;
  value: string;
  handleChange: (newValue: string | null) => void;
}> = ({ id, value, handleChange }) => (
  <input
    class="bg-paper shadow-input rounded-md border-none px-2 py-1"
    id={id}
    value={value}
    onChange={(ev) => handleChange((ev.target as any).value)}
    type="text"
  />
);
