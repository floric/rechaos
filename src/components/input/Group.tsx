import type { Component } from "solid-js";

export const Group: Component = ({ children }) => (
  <div class="grid gap-2 grid-cols-input-group items-center">{children}</div>
);
