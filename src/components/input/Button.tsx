import type { Component } from "solid-js";

export const Button: Component<{
  onClick?: () => void;
  onDragStart?: (ev: DragEvent) => void;
  draggable?: boolean;
  elevated?: boolean;
  borderless?: boolean;
}> = ({
  children,
  draggable = false,
  elevated = false,
  borderless = false,
  onClick,
  onDragStart,
}) => (
  <button
    class={`rounded-lg border ${elevated ? "shadow-button" : ""} ${
      borderless ? "border-none" : "border-gray-200"
    } box-border text-center no-underline inline-block hover:bg-gray-700 hover:bg-opacity-5 py-2 px-2 leading-none hover:text-gray-800 active:text-black`}
    onClick={() => {
      if (onClick) {
        onClick();
      }
    }}
    onDragStart={(ev) => {
      if (onDragStart) {
        onDragStart(ev);
      }
    }}
    draggable={draggable}
  >
    {children}
  </button>
);
