import { Component } from "solid-js";

export const NumericInput: Component<{
  id: string;
  value: number;
  handleChange: (newValue: number | null) => void;
}> = ({ id, value, handleChange }) => (
  <input
    class="w-24 bg-paper shadow-input rounded-md border-none px-2 py-1"
    id={id}
    value={value}
    onChange={(ev) => {
      try {
        const value: string = (ev.target as any).value;
        handleChange(Number.parseFloat(value));
      } catch (e) {
        console.error("Parsing failed", e);
      }
    }}
    type="number"
  />
);
