import { Component } from "solid-js";

export const Label: Component<{ id: string }> = ({ children, id }) => (
  <label for={id} class="text-sm">
    {children}
  </label>
);
