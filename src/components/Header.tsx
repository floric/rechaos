import { freeSet } from "@coreui/icons";
import { Component } from "solid-js";
import { setDialog } from "../stores/routes";
import { Button } from "./input/Button";
import { Icon } from "./typography/Icon";

export const Header: Component = () => (
  <div class="fixed left-0 top-0 ml-2 flex items-center leading-none">
    <div class="mr-1 select-none cursor-default">
      <span class="font-thin ">re</span>
      <span class="text-red-600 font-bold">chaos</span>
    </div>
    <div>
      <Button borderless onClick={() => setDialog("settings")}>
        <Icon icon={freeSet.cilSettings} size="sm" />
      </Button>
      <Button borderless onClick={() => setDialog("attributions")}>
        <Icon icon={freeSet.cilHeart} size="sm" />
      </Button>
    </div>
  </div>
);
