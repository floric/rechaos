import { Component } from "solid-js";
import { freeSet } from "@coreui/icons";
import { Transition } from "solid-transition-group";
import { viewport } from "../stores/viewport";
import { Icon } from "./typography/Icon";
import { onFadeEnter, onFadeExit } from "./animations/fade";

export const Infos: Component = () => (
  <Transition onEnter={onFadeEnter} onExit={onFadeExit}>
    {viewport.showZoom && (
      <div class="ui-element fixed right-0 bottom-0">
        <div class="flex items-center">
          <Icon size="sm" icon={freeSet.cilMagnifyingGlass} />
          <div class="leading-none m-0 ml-2 p-0">
            {Math.round(viewport.canvas.zoom * 100)}%
          </div>
        </div>
      </div>
    )}
  </Transition>
);
