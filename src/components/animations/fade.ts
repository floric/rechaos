import Konva from "konva";

const duration = 250;

export const fadeIn = (
  group: Konva.Group,
  drawLayer: Konva.Layer
): Promise<void> => animateOpacity(group, drawLayer, { start: 0, end: 1 });

export const fadeOut = (
  group: Konva.Group,
  drawLayer: Konva.Layer
): Promise<void> => animateOpacity(group, drawLayer, { start: 1, end: 0 });

const animateOpacity = (
  group: Konva.Group,
  drawLayer: Konva.Layer,
  transition: { start: number; end: number }
) =>
  new Promise<void>((resolve) => {
    const { start, end } = transition;
    const factor = start < end ? 1 : -1;
    group.opacity(start);
    const anim = new Konva.Animation((frame) => {
      if (!frame) {
        resolve();
        return;
      }
      const addedOpacity = frame.timeDiff / duration;
      if (factor > 0 ? group.opacity() < end : group.opacity() > end) {
        group.opacity(group.opacity() + factor * addedOpacity);
      } else {
        anim.stop();
        resolve();
      }
    }, drawLayer);
    anim.start();
  });

export const onFadeEnter = (el: Element, done: () => void): void => {
  const a = el.animate([{ opacity: 0 }, { opacity: 1 }], {
    duration,
  });
  a.finished.then(done);
};

export const onFadeExit = (el: Element, done: () => void): void => {
  const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
    duration,
  });
  a.finished.then(done);
};
