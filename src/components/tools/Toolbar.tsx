import { freeSet } from "@coreui/icons";
import { Component } from "solid-js";
import { ElementType } from "../../stores/scene";
import { Tool } from "./Tool";
import { Icon } from "../typography/Icon";

export const Toolbar: Component = () => (
  <div class="m-2 fixed bottom-0 grid grid-flow-col gap-2 justify-items-stretch place-items-stretch">
    <Tool type={ElementType.TEXT}>
      <Icon icon={freeSet.cilText} size="sm" />
    </Tool>
    <Tool type={ElementType.BOX}>
      <Icon icon={freeSet.cilSquare} size="sm" />
    </Tool>
  </div>
);
