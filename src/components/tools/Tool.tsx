import { Component } from "solid-js";
import { events, EventType } from "../../events";
import type { ElementType } from "../../stores/scene";
import { viewport } from "../../stores/viewport";
import { Button } from "../input/Button";

const onDrag = (ev: DragEvent, type: ElementType) => {
  ev.dataTransfer?.setData("elementType", type);
  ev.cancelBubble = true;
};

const onDrop = (type: ElementType) => {
  events.next({
    type: EventType.ELEMENT_ADDED,
    elementType: type,
    position: viewport.canvas.center,
  });
};

export const Tool: Component<{ type: ElementType }> = ({ type, children }) => (
  <Button
    onClick={() => onDrop(type)}
    elevated
    draggable
    onDragStart={(ev) => onDrag(ev, type)}
  >
    {children}
  </Button>
);
