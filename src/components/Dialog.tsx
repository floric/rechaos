import { freeSet } from "@coreui/icons";
import { Component, Switch, Match } from "solid-js";
import { DialogRoute, routes, setDialog } from "../stores/routes";
import { Title } from "./typography/Title";
import { Icon } from "./typography/Icon";
import { Button } from "./input/Button";

export const Dialog: Component<{ route: DialogRoute; title: string }> = ({
  route,
  title,
  children,
}) => (
  <Switch>
    <Match when={routes.openDialog === route}>
      <div class="fixed top-0 bottom-0 right-0 left-0 z-50 flex justify-center items-center bg-opacity-70 bg-gray-700">
        <div class="max-w-4xl m-4 p-4 bg-gray-50 rounded-lg">
          <div class="flex justify-between items-center">
            <Title>{title}</Title>
            <Button borderless onClick={() => setDialog(null)}>
              <Icon icon={freeSet.cilXCircle} size="m" />
            </Button>
          </div>
          <div class="mb-6" />
          {children}
        </div>
      </div>
    </Match>
  </Switch>
);
