import type { Component } from "solid-js";

export const Header: Component = ({ children }) => (
  <div class="flex justify-between items-center">{children}</div>
);
