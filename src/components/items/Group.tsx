import type { Component } from "solid-js";

export const Group: Component = (props) => (
  <div class="grid grid-cols-1 gap-4">{props.children}</div>
);
