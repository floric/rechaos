import Konva from "konva";
import { Component, createSignal, onMount } from "solid-js";
import { events, EventType } from "../events";
import { curve } from "../shapes/connection";
import {
  setCenter,
  setWidthAndHeight,
  setZoom,
  viewport,
} from "../stores/viewport";
import { createBox, updateBox } from "../shapes/box";
import { createText, updateText } from "../shapes/text";
import { updateSelection, updateHover } from "../shapes/selection";
import { updateNode } from "../shapes/util";
import { ElementType } from "../stores/scene";
import { fadeIn, fadeOut } from "./animations/fade";

const findGroup = (nodeId: string, stage: Konva.Stage) =>
  stage.findOne<Konva.Group>(`#${nodeId}`);

const findElement = <T extends Konva.Shape>(
  groupId: string,
  stage: Konva.Stage
) => stage.findOne<T>(`#${groupId}`);

const subscribeEvents = (stage: Konva.Stage, drawLayer: Konva.Layer) => {
  events.subscribe(async (ev) => {
    if (ev.type === EventType.NODE_SELECTED) {
      updateSelection(stage);
    } else if (ev.type === EventType.NODE_HOVERED) {
      updateHover(stage);
    } else if (ev.type === EventType.TEXT_EDITED) {
      const node = findElement<Konva.Text>(ev.value.shapeId, stage);
      const group = findGroup(ev.nodeId, stage);
      updateText(group, node, stage, ev.value);
    } else if (ev.type === EventType.BOX_EDITED) {
      const node = findElement<Konva.Rect>(ev.value.shapeId!, stage);
      const group = findGroup(ev.nodeId, stage);
      updateBox(group, node, stage, ev.value);
    } else if (ev.type === EventType.NODE_EDITED) {
      const group = findGroup(ev.nodeId, stage);
      updateNode(group, stage, ev.value);
    } else if (ev.type === EventType.TEXT_CREATED) {
      const { group } = createText(ev.id, ev.value.shapeId);
      drawLayer.add(group);
      fadeIn(group, drawLayer);
      events.next({
        type: EventType.TEXT_EDITED,
        nodeId: ev.id,
        value: ev.value,
      });
      events.next({
        type: EventType.NODE_SELECTED,
        nodeId: ev.id,
        isExpanded: false,
      });
    } else if (ev.type === EventType.BOX_CREATED) {
      const { group } = createBox(ev.id, ev.value.shapeId);
      drawLayer.add(group);
      fadeIn(group, drawLayer);
      events.next({
        type: EventType.BOX_EDITED,
        nodeId: ev.id,
        value: ev.value,
      });
      events.next({
        type: EventType.NODE_SELECTED,
        nodeId: ev.id,
        isExpanded: false,
      });
    } else if (ev.type === EventType.NODE_DELETED) {
      const group = findGroup(ev.nodeId, stage);
      await fadeOut(group, drawLayer);
      group.remove();
    }
  });
};

const initStage = () => {
  setCenter({
    x: 0,
    y: 0,
  });
  const stage = new Konva.Stage({
    container: "container",
    width: window.innerWidth,
    height: window.innerHeight,
    draggable: true,
    x: window.innerWidth / 2,
    y: window.innerHeight / 2,
  });
  setWidthAndHeight(stage.width(), stage.height());
  window.addEventListener("resize", () => {
    stage.width(window.innerWidth).height(window.innerHeight);
    setWidthAndHeight(stage.width(), stage.height());
    // TODO handle resize
  });

  stage
    .on("click", (ev) => {
      events.next({
        type: EventType.NODE_SELECTED,
        nodeId: null,
        isExpanded: false,
      });
      ev.cancelBubble = true;
    })
    .on("dragmove", (ev: any) => {
      ev.cancelBubble = true;
      document.body.style.cursor = "grabbing";
    })
    .on("dragend", (ev: any) => {
      setCenter({
        x: -stage.position().x + window.innerWidth / 2,
        y: -stage.position().y + window.innerHeight / 2,
      });
      ev.cancelBubble = true;
      document.body.style.cursor = "default";
    });

  const drawLayer = new Konva.Layer();
  stage.add(drawLayer);

  // TODO remove after test
  drawLayer.add(curve);

  return {
    stage,
    drawLayer,
  };
};

const onDragOver = (ev: DragEvent) => {
  ev.preventDefault();
};

const onDrop = (
  ev: DragEvent,
  layer: Konva.Layer | null,
  stage: Konva.Stage | null
) => {
  ev.preventDefault();
  if (!layer || !stage) {
    console.warn("UI not ready yet");
    return;
  }

  const elementType = ev.dataTransfer?.getData("elementType") as ElementType;
  events.next({
    type: EventType.ELEMENT_ADDED,
    elementType,
    position: {
      x: (ev.clientX - stage.position().x) / viewport.canvas.zoom,
      y: (ev.clientY - stage.position().y) / viewport.canvas.zoom,
    },
  });
};

export const Canvas: Component = () => {
  const [drawLayer, setDrawLayer] = createSignal<Konva.Layer | null>(null);
  const [stage, setStage] = createSignal<Konva.Stage | null>(null);

  onMount(() => {
    const { drawLayer, stage } = initStage();
    setDrawLayer(drawLayer);
    setStage(stage);
    subscribeEvents(stage, drawLayer);
  });

  return (
    <div
      id="container"
      onDrop={(ev) => onDrop(ev, drawLayer(), stage())}
      onDragOver={onDragOver}
      onWheel={(e) => {
        // prevent zoom into UI
        e.preventDefault();

        const currentDrawLayer = drawLayer();
        const currentStage = stage();

        if (!currentDrawLayer || !currentStage) {
          return <div>Loading</div>;
        }

        // allow zoom between 20 and 500 percent
        const factor = -e.deltaY * 0.0005;
        const zoom = Math.min(5, Math.max(0.1, viewport.canvas.zoom + factor));
        const change = zoom - viewport.canvas.zoom;
        setZoom(zoom);

        // zoom into center
        currentDrawLayer.scale({ x: zoom, y: zoom });
        currentStage.setPosition({
          x: currentStage.position().x - change * viewport.canvas.center.x,
          y: currentStage.position().y - change * viewport.canvas.center.y,
        });

        events.next({ type: EventType.ZOOM, zoom });
      }}
    />
  );
};
