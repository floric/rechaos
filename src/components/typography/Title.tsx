import type { Component } from "solid-js";

export const Title: Component = ({ children }) => (
  <h2 class="text-xl font-bold uppercase">{children}</h2>
);
