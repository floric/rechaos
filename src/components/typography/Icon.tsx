import { Component } from "solid-js";

export const Icon: Component<{ icon: any[]; size: "m" | "sm" }> = ({
  icon,
  size,
}) => {
  const [dimensions, path] = icon;
  return (
    <svg
      class={`${size === "m" ? "w-6 h-6" : "w-4 h-4"} m-0 p-0 leading-none`}
      viewBox={`0 0 ${dimensions}`}
      xmlns="http://www.w3.org/2000/svg"
      innerHTML={path}
    ></svg>
  );
};
