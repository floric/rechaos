import type { Component } from "solid-js";

export const Subtitle: Component = ({ children }) => (
  <h3 class="text-xs font-light uppercase mb-2">{children}</h3>
);
