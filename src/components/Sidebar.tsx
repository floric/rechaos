import { Component, createMemo, Switch, Match } from "solid-js";
import { freeSet } from "@coreui/icons";
import { Transition } from "solid-transition-group";
import { Text } from "./settings/Text";
import { Box } from "./settings/Box";
import { Button } from "./input/Button";
import { Group } from "./items/Group";
import { Header } from "./items/Header";
import { ElementType, currentNode, TextNode, BoxNode } from "../stores/scene";
import { closeProperties, openProperties, viewport } from "../stores/viewport";
import { onFadeEnter, onFadeExit } from "./animations/fade";
import { Subtitle } from "./typography/Subtitle";
import { Title } from "./typography/Title";
import { Icon } from "./typography/Icon";
import { events, EventType } from "../events";

import "./styles/basic.css";

const showCollapsedSidebar = createMemo(
  () => currentNode() !== null && !viewport.isPropertiesOpen
);
const showExpandedSidebar = createMemo(
  () => currentNode() !== null && viewport.isPropertiesOpen
);

export const Sidebar: Component = () => (
  <Transition onEnter={onFadeEnter} onExit={onFadeExit}>
    {currentNode() && (
      <div class="ui-element fixed right-0 top-0">
        <Switch>
          <Match when={showCollapsedSidebar()}>
            <Group>
              <Header>
                <Title>{currentNode()!.title}</Title>
                <div class="ml-4">
                  <Button borderless onClick={openProperties}>
                    <Icon icon={freeSet.cilArrowCircleLeft} size="sm" />
                  </Button>
                </div>
              </Header>
            </Group>
          </Match>
          <Match when={showExpandedSidebar()}>
            <Group>
              <Header>
                <Title>{currentNode()!.title}</Title>
                <div class="ml-4">
                  <Button borderless onClick={closeProperties}>
                    <Icon icon={freeSet.cilArrowCircleRight} size="sm" />
                  </Button>
                </div>
              </Header>
              <div>
                <Subtitle>Actions</Subtitle>
                <div>
                  <Button
                    onClick={() => {
                      events.next({
                        type: EventType.NODE_DELETED,
                        nodeId: currentNode()!.id,
                      });
                    }}
                  >
                    <span class="text-red-600">
                      <Icon icon={freeSet.cilTrash} size="sm" />
                    </span>
                  </Button>
                </div>
              </div>
              <div>
                <Subtitle>Properties</Subtitle>
                <div>
                  {currentNode()!.type === ElementType.TEXT ? (
                    <Text node={currentNode() as TextNode} />
                  ) : null}
                  {currentNode()!.type === ElementType.BOX ? (
                    <Box node={currentNode() as BoxNode} />
                  ) : null}
                </div>
              </div>
            </Group>
          </Match>
        </Switch>
      </div>
    )}
  </Transition>
);
