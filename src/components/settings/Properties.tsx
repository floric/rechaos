import type { Component } from "solid-js";

export const Properties: Component = ({ children }) => (
  <div class="grid gap-4">{children}</div>
);
