import type { Component } from "solid-js";
import { events, EventType } from "../../events";
import type { TextNode } from "../../stores/scene";
import { Group } from "../input/Group";
import { Label } from "../input/Label";
import { NumericInput } from "../input/NumericInput";
import { TextInput } from "../input/TextInput";
import { Properties } from "./Properties";

const handleTextChange = (newValue: string | null, node: TextNode) => {
  node.value = newValue || "";
  triggerUpdate(node);
};

const handleFontSizeChange = (newValue: number, node: TextNode) => {
  node.fontSize = Math.ceil(newValue) || 12;
  triggerUpdate(node);
};

const triggerUpdate = (node: TextNode) => {
  events.next({
    type: EventType.TEXT_EDITED,
    nodeId: node.id,
    value: node,
  });
};

export const Text: Component<{ node: TextNode }> = ({ node }) => (
  <Properties>
    <Group>
      <Label id="text">Text</Label>
      <TextInput
        id="text"
        value={node.value}
        handleChange={(val) => handleTextChange(val, node)}
      />
    </Group>
    <Group>
      <Label id="font-size">Font Size</Label>
      <NumericInput
        id="font-size"
        value={node.fontSize}
        handleChange={(val) => handleFontSizeChange(val || 12, node)}
      />
    </Group>
  </Properties>
);
