import { Component } from "solid-js";
import { freeSet } from "@coreui/icons";
import { events, EventType } from "../../events";
import type { BoxNode } from "../../stores/scene";
import { Group } from "../input/Group";
import { Label } from "../input/Label";
import { NumericInput } from "../input/NumericInput";
import { Icon } from "../typography/Icon";
import { Properties } from "./Properties";

const handleWidthChange = (newValue: number, node: BoxNode) => {
  node.width = newValue;
  triggerUpdate(node);
};

const handleHeightChange = (newValue: number, node: BoxNode) => {
  node.height = newValue;
  triggerUpdate(node);
};

const triggerUpdate = (node: BoxNode) => {
  events.next({
    type: EventType.BOX_EDITED,
    nodeId: node.id,
    value: node,
  });
};

export const Box: Component<{ node: BoxNode }> = ({ node }) => (
  <Properties>
    <Group>
      <Label id="text">Dimensions</Label>
      <div class="flex items-center">
        <NumericInput
          id="text"
          value={node.width}
          handleChange={(val) => handleWidthChange(val || 100, node)}
        />
        <div class="mx-2">
          <Icon icon={freeSet.cilX} size="sm" />
        </div>
        <NumericInput
          id="text"
          value={node.height}
          handleChange={(val) => handleHeightChange(val || 100, node)}
        />
      </div>
    </Group>
  </Properties>
);
