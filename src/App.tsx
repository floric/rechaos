import type { Component } from "solid-js";
import { Canvas } from "./components/Canvas";
import { Header } from "./components/Header";
import { Sidebar } from "./components/Sidebar";
import { Toolbar } from "./components/tools/Toolbar";
import { Infos } from "./components/Infos";
import { Attributions } from "./pages/Attributions";
import { Settings } from "./pages/Settings";

const App: Component = () => (
  <main
    class="select-none"
    onWheel={(e) => {
      // prevent zoom into UI
      e.preventDefault();
    }}
  >
    <Canvas />
    <Toolbar />
    <Sidebar />
    <Header />
    <Infos />
    <Attributions />
    <Settings />
  </main>
);

export default App;
